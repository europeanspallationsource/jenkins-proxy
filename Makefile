.PHONY: help build tag push clean release test test_image run

OWNER := europeanspallationsource
GIT_TAG := $(shell git describe --always)
IMAGE := jenkins-proxy


help:
# http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
	@echo "jenkins-proxy"
	@echo "======="
	@echo
	@grep -E '^[a-zA-Z0-9_%/-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## build the latest image
	docker build -t $(OWNER)/$(IMAGE):latest .

tag: ## tag the latest image with the git tag
	docker tag $(OWNER)/$(IMAGE):latest $(OWNER)/$(IMAGE):$(GIT_TAG)

push: ## push the latest and git tag image
	docker push $(OWNER)/$(IMAGE):$(GIT_TAG)
	docker push $(OWNER)/$(IMAGE):latest

clean: ## remove the image with git tag and the test database
	-docker rmi $(OWNER)/$(IMAGE):$(GIT_TAG)

release: build \
	tag \
	push
release: ## build, tag, and push all stacks

test:  ## run the tests (on current directory)
	docker run --rm -v ${CURDIR}:/app $(OWNER)/$(IMAGE):latest pytest --cov=app -v

test_image:  ## run the tests (on the latest image)
	docker run --rm $(OWNER)/$(IMAGE):latest pytest --cov=app -v

run: ## run the application in debug mode
	docker run --rm -p 5000:5000 -e FLASK_DEBUG=1 -v ${CURDIR}:/app -i -t $(OWNER)/$(IMAGE):latest flask run --host 0.0.0.0 --port 5000

run_uwsgi: ## run the application with uwsgi
	docker run --rm -p 5000:5000 -v ${CURDIR}:/app -i -t $(OWNER)/$(IMAGE):latest
