pipeline {
    agent { label 'docker-ce' }

    environment {
        GIT_TAG = sh(returnStdout: true, script: 'git describe --exact-match || true').trim()
    }

    stages {
        stage('Build') {
            steps {
                slackSend (color: 'good', message: "STARTED: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
                sh 'make clean'
                ansiColor('xterm') {
                    sh 'make build'
                }
            }
        }
        stage('Test') {
            steps {
                sh 'make test_image'
            }
        }
        stage('Push') {
            when {
                not { environment name: 'GIT_TAG', value: '' }
            }
            environment {
                DOCKERHUB = credentials('dockerhub')
            }
            steps {
                sh 'docker login -u $DOCKERHUB_USR -p $DOCKERHUB_PSW'
                sh 'make tag'
                sh 'make push'
                sh 'docker logout'
            }
        }
    }

    post {
        always {
            sh 'make clean'
            /* clean up the workspace */
            deleteDir()
        }
        failure {
            slackSend (color: 'danger', message: "FAILED: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
        }
        success {
            slackSend (color: 'good', message: "SUCCESSFUL: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
        }
    }

}
