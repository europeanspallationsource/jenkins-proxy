#
# EPICS Environment Manager
# Copyright (C) 2015 Cosylab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This script generates the correct module version string from the sources. If
the code is in a git repository, use the information from the repository.
"""

import re


class SemverVersion(object):  # pylint: disable=too-few-public-methods
    """Container object for Semver version"""
    def __init__(self, version, prerelease='', metadata=''):
        self.major = version[0]
        self.minor = version[1]
        self.patch = version[2]
        self.prerelease = '-{}'.format(prerelease) if prerelease else ''
        self.metadata = '-{}'.format(metadata) if metadata else ''

    def __repr__(self):
        return '{}.{}.{}{}{}'.format(self.major, self.minor, self.patch, self.prerelease, self.metadata)

    def __eq__(self, other):
        return (self.major, self.minor, self.patch, self.prerelease, self.metadata) == (
            other.major, other.minor, other.patch, other.prerelease, other.metadata)

    def __lt__(self, other):
        if (self.major, self.minor, self.patch) == (other.major, other.minor, other.patch):
            if not self.prerelease:
                # Not having a prerelease tag means that 'self' always is not lesser
                return False
            if not other.prerelease:
                # Not having a prerelease tag means that 'other' always is lesser
                return True
            # If both have prerelease tags, order dot-separated identifiers alphabetically
            return self.prerelease.split('.') < other.prerelease.split('.')
        else:
            return (self.major, self.minor, self.patch) < (other.major, other.minor, other.patch)


def parse_tag(tag, site_prefix):
    """Parses tag according to semver, taking into account a site specific prefix"""
    major = 0
    minor = 0
    patch = 0
    prerelease = ''
    metadata = ''

    matches = re.search(r'\+([A-Za-z0-9.\-]+)', tag)
    if matches:
        metadata = matches.group(1)

    matches = re.search(r'-([A-Za-z0-9.\-]+)', tag)
    if matches:
        prerelease = matches.group(1)

    matches = re.match(site_prefix + r'(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)', tag)
    if matches:
        major = matches.group(1)
        minor = matches.group(2)
        patch = matches.group(3)
    else:
        matches = re.match(site_prefix + r'(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)', tag)
        if matches:
            major = matches.group(1)
            minor = matches.group(2)
        else:
            matches = re.match(site_prefix + r'(0|[1-9][0-9]*)', tag)
            if matches:
                major = matches.group(1)
            else:
                return None
    return SemverVersion((major, minor, patch), prerelease, metadata)
