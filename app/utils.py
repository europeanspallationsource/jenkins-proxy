# -*- coding: utf-8 -*-
"""
app.utils
~~~~~~~~~

This module implements utility functions.

:copyright: (c) 2018 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
import urllib.parse
from flask import current_app


class ProxyError(Exception):
    """ProxyError class

    Exception used to return HTTP status code and error message
    """
    status_code = 400

    def __init__(self, message, status_code=None):
        super().__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code

    def __str__(self):
        return self.message


def validate_jenkins_url(jenkins_url):
    jenkins_hostname = urllib.parse.urlparse(jenkins_url).hostname
    if jenkins_hostname is None:
        raise ProxyError('jenkins query string parameter is required', 400)
    elif not jenkins_hostname.endswith(current_app.config['ALLOWED_JENKINS_DOMAINS']):
        raise ProxyError(f'The jenkins url "{jenkins_url}" is not allowed.', 403)


def get_bitbucket_info(data):
    repo_url = data['repository']['links']['html']['href']
    username = data['actor']['username']
    return (repo_url, username)
